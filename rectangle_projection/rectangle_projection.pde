//import processing.opengl.*;

// green line: color(34, 136, 34)
// red: color(136, 34, 34)

final int frameWidth = 255;
final int frameHeight = 255;
final int pointDiameter = 5;
final float halfDiameter = pointDiameter/2;

final int boxHalfWidth = 33;
final int boxHalfHeight = 25;

PVector centerPoint  = new PVector(frameWidth/2, frameHeight/2);
PVector controlPoint = new PVector(frameWidth/2, frameHeight-10);

// Angle in radians of the angle from the center point to the control point
float cpAngle = 0;
final int cpRadius = 110; // distance from center point to control point

int selectedIndex = -1;
ArrayList<PVector> controlPoints = new ArrayList<PVector>();

void setup() {
  size(frameWidth, frameHeight);
  background(255);  
  frameRate(60);
  
  relocateControlPoint();
  controlPoints.add(controlPoint);
}

void draw() {
  background(255);
  
  strokeWeight(1);  
  drawCenterBox();
  drawControlPoint();    
  drawAxis();
    
  fill(0);
  text("FPS: " + frameRate, frameWidth-100, 15);
}

void drawCenterBox() {
   rectMode(RADIUS);
   stroke(34, 34, 136);
   fill(187, 187, 213);
   rect(centerPoint.x, centerPoint.y, boxHalfWidth, boxHalfHeight);
   noStroke();
   fill(148, 148, 193);
   rect(centerPoint.x, centerPoint.y, 1, 1);
}

void drawAxis() {
  // perpendicular lines have inverse and opposite slope or inverse reciprocal
  PVector axisSlope = new PVector(-(controlPoint.y-centerPoint.y), controlPoint.x-centerPoint.x);  
  axisSlope.mult(1000);  
  stroke(150);
  line(controlPoint.x-axisSlope.x, controlPoint.y-axisSlope.y, controlPoint.x+axisSlope.x, controlPoint.y+axisSlope.y);
}

void drawControlPoint() {
  noStroke();
  fill(136, 34, 34);
  rectMode(RADIUS);
  rect(controlPoint.x, controlPoint.y, 3, 3);
}

void mousePressed() {
  for (int i = 0; i < controlPoints.size(); i++) {
    PVector p = (PVector)controlPoints.get(i);
    float clickSize = halfDiameter + 2;    
    if (mouseX > (p.x - clickSize) && mouseX < (p.x + clickSize) &&
        mouseY > (p.y - clickSize) && mouseY < (p.y + clickSize)) {
      selectedIndex = i;
      return;
    } 
  }
  selectedIndex = -1; 
}

void mouseDragged() {
  if (selectedIndex >= 0) {
    cpAngle = atan2(mouseX - centerPoint.x, mouseY - centerPoint.y);
    relocateControlPoint();
  }
}

void relocateControlPoint() {  
  controlPoint.x = centerPoint.x + (sin(cpAngle)*cpRadius);
  controlPoint.y = centerPoint.y + (cos(cpAngle)*cpRadius);
}

void arrow(PVector start, PVector end) {
  line(start.x, start.y, end.x, end.y);
  pushMatrix();
  translate(end.x, end.y);
  float a = atan2(start.x-end.x, end.y-start.y);
  rotate(a);
  int tailLength = 5;
  line(0, 0, -tailLength, -tailLength);
  line(0, 0, tailLength, -tailLength);
  popMatrix();
} 

