import processing.opengl.*;

final int frameWidth = 800;
final int frameHeight = 600;
final int pointDiameter = 7;
final float halfDiameter = pointDiameter/2;

int selectedIndex = -1;

PVector p0 = new PVector(300, 300);
PVector p1 = new PVector(700, 500);
PVector p2 = new PVector(100, 500);
ArrayList<PVector> controlPoints = new ArrayList<PVector>();

PVector sourceVector;
PVector projectedVector;

void setup() {
  size(frameWidth, frameHeight);
  background(255);  
  frameRate(60);
  
  controlPoints.add(p0);
  controlPoints.add(p1);
  controlPoints.add(p2);
    
  sourceVector = p0;
  projectedVector = projectVector(p1, p0, p1, p2);  
}

void draw() {
  background(255);
    
  drawAxis(p1, p2);  
  drawPoints();
  drawSourceVector();
  drawProjectedVector();
    
  text("FPS: " + frameRate, 680, 15);
  text("Projected Vector: [" + projectedVector.x + ", " + projectedVector.y + "]", 10, 10);
}

void drawPoints() {  
  textFont(loadFont("CourierNewPSMT-11.vlw"));
  int labelOffset = 6;
  
  rectMode(CENTER);  
  noStroke();
  fill(0, 0, 255);
  rect(p0.x, p0.y, pointDiameter, pointDiameter);
  text("p0", p0.x + labelOffset, p0.y + labelOffset);
  
  fill(200, 0, 0);
  rect(p1.x, p1.y, pointDiameter, pointDiameter);
  text("p1", p1.x + labelOffset, p1.y + labelOffset);
  
  rect(p2.x, p2.y, pointDiameter, pointDiameter);
  text("p2", p2.x + labelOffset, p2.y + labelOffset);
}

void drawAxis(PVector p1, PVector p2) {
  float dx = p1.x - p2.x;
  float dy = p1.y - p2.y;
  
  float multiplier = 1000;
  float xStart = p1.x + (dx * multiplier);
  float yStart = p1.y + (dy * multiplier);
  float xEnd = p2.x + (dx * -multiplier);
  float yEnd = p2.y + (dy * -multiplier);
  
  strokeWeight(1);
  stroke(150);
  line(xStart, yStart, xEnd, yEnd);
}

void drawSourceVector() {
  stroke(100, 100, 200);
  strokeWeight(2);
  arrow(p1, sourceVector);
}

void drawProjectedVector() {
  stroke(0, 255, 0);
  strokeWeight(2);
  arrow(p1, projectedVector);
  
  strokeWeight(1);
  stroke(220);
  line(p0.x, p0.y, projectedVector.x, projectedVector.y);
  
  rightAngleMark(p0, projectedVector);
}

// Project the vector formed by vStart, vEnd onto the axis that passes through axis1 and axis2
PVector projectVector(PVector vStart, PVector vEnd, PVector axis1, PVector axis2) {
  PVector a = new PVector(vStart.x - vEnd.x, vStart.y - vEnd.y); 
  PVector b = new PVector(axis1.x - axis2.x, axis1.y - axis2.y);
  PVector proj = new PVector();
  
  float dp = -a.dot(b); //dot product
  proj.x = (dp / (b.x*b.x + b.y*b.y)) * b.x;
  proj.y = (dp / (b.x*b.x + b.y*b.y)) * b.y;
  
  proj.add(p1);
  
  return proj;
}

void mousePressed() {
  for (int i = 0; i < controlPoints.size(); i++) {
    PVector p = (PVector)controlPoints.get(i);
    float clickSize = halfDiameter + 2;    
    if (mouseX > (p.x - clickSize) && mouseX < (p.x + clickSize) &&
        mouseY > (p.y - clickSize) && mouseY < (p.y + clickSize)) {
      selectedIndex = i;
      return;
    } 
  }
  selectedIndex = -1; 
}

void mouseDragged() {
  if (selectedIndex >= 0) {
    PVector p = controlPoints.get(selectedIndex);
    p.x = mouseX;
    p.y = mouseY;
    projectedVector = projectVector(p1, p0, p1, p2);
  }
}

void arrow(PVector start, PVector end) {  
  line(start.x, start.y, end.x, end.y);
  pushMatrix();
  translate(end.x, end.y);
  float a = atan2(start.x-end.x, end.y-start.y);  
  rotate(a);
  int tailLength = 5;
  line(0, 0, -tailLength, -tailLength);
  line(0, 0, tailLength, -tailLength);
  popMatrix();
} 

void rightAngleMark(PVector start, PVector end) {    
  pushMatrix();
  translate(end.x, end.y);
  float a = atan2(start.x-end.x, end.y-start.y);
  rotate(a);
  int tailLength = 10;
  line(tailLength, 0, tailLength, -tailLength);
  line(tailLength, -tailLength, 0, -tailLength);
  popMatrix();
}
